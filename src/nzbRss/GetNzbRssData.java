/********************************************************************************************
 * GetNzbRssData.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 10, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package nzbRss;

import base.MyNzbRssList;
import base.Nzb;
import myEpisodesData.Show;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class GetNzbRssData implements Runnable
{
    private URL link = null;
    private volatile MyNzbRssList syncList = null;
    
    public GetNzbRssData(MyNzbRssList list)
    {
        syncList = list;
                
        // Create the search url
        String searchString = null;
        
        // Fix the series name
        String series = list.getShow().getSeriesName().replaceAll("[" + list.getJson().getReplacement() + "]", "").replaceAll("\\s", list.getJson().getSeparator());

        // Regex stuff
        String pattern = "\\/[a-zA-Z]+\\/([%a-zA-Z0-9]+)";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(list.getJson().getLink());
        if(m.find())
            searchString = list.getJson().getLink().replaceAll(m.group(1), series);

        else
            searchString = list.getJson().getLink().replaceAll("%SERIESNAME%", series);

        // Log the data
        LogManager.getLogger("info").info(searchString);
        
        try 
        {
            link = new URL(searchString);
        }
        catch(MalformedURLException ex) 
        {
            LogManager.getLogger().error(GetNzbRssData.class.getName(), ex);
        }
    }
    
    @Override
    public void run() 
    {
        File tempFile = null;
        try 
        {
            synchronized(syncList)
            {
                tempFile = File.createTempFile(syncList.getJson().getName(), ".temp");
                tempFile.deleteOnExit();
                FileUtils.copyURLToFile(link, tempFile); 
            }
        }
        catch(IOException ex) 
        {
            LogManager.getLogger().error(GetNzbRssData.class.getName(), ex);
        }
        
        // Get the show
        Show show = null;
        synchronized(syncList)
        {
            show = syncList.getShow();
        }
        
        // Create a new model
        DefaultListModel model = new DefaultListModel();
	
	try 
        {
            SAXReader reader = new SAXReader();
            Document document = reader.read(tempFile);
            List<Node> nodes = document.selectNodes("/rss/channel/item");
            
            for(Node node : nodes)
            {
                if(node.getName().equalsIgnoreCase("item"))
                {
                    // Do checks to see that this item is of the proper list
                    // Data should look like:
                    // <![CDATA[ Vikings Season 4 Episode 4<br />Size: 3.48 GB ]]>
                    Pattern r = Pattern.compile("Season ([0-9]*) Episode ([0-9]*)");
                    Matcher m = r.matcher(node.selectSingleNode("description").getText());
                    
                    if(m.find())
                    {
                        // Check season and episode
                        if(show.getSeasonNumber().equals(String.valueOf(Integer.parseInt(m.group(1)))) && show.getEpisodeNumber().equals(m.group(2)))
                        {
                            // Add the nzb to the top level and keep the next level to add to
                            model.addElement(new Nzb(node));
                        }
                    }
                }
            }
        }
        catch(DocumentException ex) 
        {
            LogManager.getLogger().error(GetNzbRssData.class.getName(), ex);
        }
        
        synchronized(syncList)
        {
            if(model.getSize() < 1)
                model.addElement("Empty...");
            
            syncList.setModel(model);
        }
    }
}
