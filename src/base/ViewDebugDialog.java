/********************************************************************************************
 * ViewDebugDialog.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 09, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package base;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author ZorDerelda
 */
public class ViewDebugDialog extends java.awt.Dialog
{
    /**
     * Creates new form ViewDebug
     * @param parent
     */
    public ViewDebugDialog(MyEpisodesGrabber parent)
    {
        super(parent, true);
        initComponents();
        
        // Set the renderer
        jList.setCellRenderer(new MyListCellRenderer());
        
        try
        {
            // Load the file and get the list
            List<String> lines = FileUtils.readLines(new File("MyEpisodesLogger.log"), "UTF-8");
            
            // Create a new model
            DefaultListModel model = new DefaultListModel();
            
            // Loops
            for(String line : lines) 
            {
                // Add the string to the list
                model.addElement(line);
            }
            
            jList.setModel(model);
        }
        catch(IOException ex)
        {
            LogManager.getLogger().error(ViewDebugDialog.class.getName(), ex);
        }
        
        jList.addMouseListener(new MouseAdapter()
        { 
            @Override
            public void mouseClicked(MouseEvent e) 
            {
                JList list = (JList)e.getSource();
                int index = list.locationToIndex(e.getPoint());
                
                if(SwingUtilities.isLeftMouseButton(e))
                {
                    if(e.getClickCount() == 2)
                    {
                        if(list.getModel().getElementAt(index) instanceof String)
                        {
                            String link = jList.getSelectedValue();
                
                            // Open the webpage
                            if(Desktop.isDesktopSupported())
                            {
                                Desktop desktop = Desktop.getDesktop();
                                try
                                {
                                    desktop.browse(new URI(link));
                                }
                                catch(IOException | URISyntaxException ex) 
                                {
                                    LogManager.getLogger().error(ViewDebugDialog.class.getName(), ex);
                                }
                            }

                            else
                            {
                                Runtime runtime = Runtime.getRuntime();
                                try
                                {
                                    runtime.exec("xdg-open " + link);
                                }
                                catch(IOException ex) 
                                {
                                    LogManager.getLogger().error(ViewDebugDialog.class.getName(), ex);
                                }
                            }
                        }
                    }
                }
                
                // Right Click && not popped up
                else if(SwingUtilities.isRightMouseButton(e) && !e.isPopupTrigger())
                {
                    // The popup menu
                    JPopupMenu popup = new JPopupMenu();
                    
                    JMenuItem menuItem = new JMenuItem("Erase?");
                    menuItem.addActionListener((ActionEvent evt)->
                    {
                        try
                        {
                            FileUtils.write(new File("MyEpisodesLogger.log"), "", "UTF-8");
                            jList.setModel(new DefaultListModel());
                        }
                        catch(IOException ex)
                        {
                            Logger.getLogger(ViewDebugDialog.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                    popup.add(menuItem);

                    // Show the menu
                    popup.show((JComponent)e.getSource(), e.getX(), e.getY());
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList = new javax.swing.JList<>();

        setModal(true);
        setPreferredSize(new java.awt.Dimension(1000, 450));
        setTitle("Debug Log");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                closeDialog(evt);
            }
        });

        jList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jList);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> jList;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}

class MyListCellRenderer extends DefaultListCellRenderer 
{
    public MyListCellRenderer() 
    {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) 
    {
        // Assumes the stuff in the list has a pretty toString
        setText(value.toString());
        
        Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        // based on the index you set the color.  This produces the every other effect.
        if(index % 2 == 0)
            c.setBackground(Color.YELLOW);
        else
            c.setBackground(Color.WHITE);

        return this;
    }
}
