/********************************************************************************************
 * Nzb.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 09, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

// Package
package base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.dom4j.Node;

public class Nzb 
{
    // Primary information
    protected String name = new String();
    protected String guid = new String();
    protected String comments = new String();
    protected String date = new String();  
    protected String size = new String();    
    
    // Attributes
    protected HashMap<String, String> attributes = new HashMap<>();
    
    public Nzb()
    {
    }
    
    public Nzb(Node node)
    {
        // The name
	name = node.selectSingleNode("title").getText();
	
	// Get the guid
	Pattern r = Pattern.compile("\\/([a-zA-Z0-9]+)$");
	Matcher m = r.matcher(node.selectSingleNode("guid").getText());
        
	if(m.find())
	{
	    guid = m.group(1);
	}
        else
        {
            // Get GUID from Nzb-Rss
            guid = node.selectSingleNode("link").getText();
        }
	
	// Get the comments link
        try
        {
            comments = node.selectSingleNode("comments").getText();
        }
        catch(NullPointerException ex)
        {
            comments = "";
        }
	
	// Get the provider date
        try
        {
            date = node.selectSingleNode("date").getText();
        }
        catch(NullPointerException ex)
        {
            date = node.selectSingleNode("pubDate").getText();
        }
	
        // Get the size
        if(!node.selectSingleNode("enclosure").valueOf("@length").isEmpty())
            size = node.selectSingleNode("enclosure").valueOf("@length");
        
        // Get the newznab:attr
        List<Node> nodes = node.selectNodes("//newznab:attr");
        
        for(Node single : nodes)
        {
            attributes.put(single.valueOf("@name"), single.valueOf("@value"));
        }
    }
    
    /**
	Method to get the guid
	@return  String
    */
    public String getGuid() { return guid; }
    
    /**
	Method to get the web browser link to the comments
	@return String
    */
    public String getComments() { return comments; }
    
    /**
	Overloaded toString method
	@return String
    */
    @Override
    public String toString() { return name; }
    
    /**
	Method to get the tool tip
	@return String
    */
    public String getToolTip()
    {
	// Develop the tool tip
	String tooltip = "<html>" +
		"<table border=1>" +
		    "<tr>" +
			"<td align=right>Size</td>" +
			"<td align=left>" + convertFromBytes() + "</td>" +
		    "</tr>" +
		    "<tr>" +
			"<td align=right>Date</td>" +
			"<td align=left>" + date + "</td>" +
		    "</tr>";
        
        for(Map.Entry<String, String> entry : attributes.entrySet()) 
        {   
            tooltip += "<tr>" +
                "<td align=right>" + entry.getKey() + "</td>" +
                "<td align=left>" + entry.getValue() + "</td>" +
            "</tr>";
        }
		
        tooltip += "</table>" +
	    "</html>";
	
	return tooltip; 
    }
    
    /**
     * Lists the attributes available from the list
     * @param att
     * @return 
     */
    public String getAttribute()
    {
        String output = "";
        
        for(Map.Entry<String, String> entry : attributes.entrySet()) 
            output += entry.getKey() + "|";
        
        return output;
    }
    
    /**
     * Gets an attribute from the list
     * @param att
     * @return 
     */
    public String getAttribute(String att)
    {
        if(attributes.containsKey(att))
            return attributes.get(att);
        
        return "Not available";
    }
    
    /**
	Converts the size to a human readable version
	@return String
    */
    private String convertFromBytes()
    {
	final long byteConversion = 1024L;
	double bytes = Double.valueOf(size);
		
	// TB Range
	if(bytes >= Math.pow(byteConversion, 4))
	    return String.format("%.2f TB", bytes / Math.pow(byteConversion, 4));

	// GB Range
	else if(bytes >= Math.pow(byteConversion, 3))
	    return String.format("%.2f GB", bytes / Math.pow(byteConversion, 3));

	// MB Range
	else if(bytes >= Math.pow(byteConversion, 2))
		return String.format("%.2f MB", bytes / Math.pow(byteConversion, 2));

	// KB Range
	else if(bytes >= byteConversion)
		return String.format("%.2f KB", bytes / byteConversion);

	// Bytes
	else
	    return String.format("%.2f BY", bytes);
    }
}
