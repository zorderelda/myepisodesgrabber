/********************************************************************************************
 * MyEpisodesTree.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 07, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * v1.02.*
 * - Removed unused imports
 * - Changed from an action listener to a runnable to remove lag on the popup
 * 
********************************************************************************************/

package base;

import configs.ApplicationJson;
import configs.MyEpisodesJson;
import configs.NewzNabJson;
import configs.NzbRssJson;
import myEpisodesData.Show;
import newzNabData.GetNewzNabData;
import nzbRss.GetNzbRssData;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import myEpisodesData.tvMazeJson.TvMazeJson;
import org.apache.logging.log4j.LogManager;

public class MyEpisodesTree extends JTree
{
    private ApplicationJson appObj = null;
    private MyEpisodesJson myepjson = null;
    private JScrollPane tabbedPane = null;
    
    public MyEpisodesTree(ApplicationJson app, JScrollPane pane)
    {
	// Call to super
	super();
        
        // Copy the appobject
        appObj = app;
        
        // Copy the my eps json
        myepjson = appObj.getMyEpisodes();
        
        // Copy the scroll pane
        tabbedPane = pane;
        
        // Set only to single selection
        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        
        // Add mouse adapter
        this.addMouseListener(new MouseAdapter()
        {           
            @Override
            public void mouseClicked(MouseEvent e) 
            {
                // Get the tree
                MyEpisodesTree parent = (MyEpisodesTree)e.getSource();

                // Set the clicked item as being assigned
                parent.setSelectionRow(parent.getClosestRowForLocation(e.getX(), e.getY()));

                // Get the node
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)parent.getLastSelectedPathComponent();

                // If nothing is selected
                if(node == null)
                    return;

                // Check to make sure its a show
                if(node.getUserObject() instanceof Show)
                {
                    // Get the show data
                    Show show = (Show)node.getUserObject();
                        
                    // Left Click
                    if(SwingUtilities.isLeftMouseButton(e))
                    {
                        // Get the application json object
                        ApplicationJson appObj = parent.getAppJsonObject();

                        // Get myeps json
                        MyEpisodesJson myepsJson = appObj.getMyEpisodes();

                        // Add the NewzNabNzb-Rss Tab
                        NzbRssJson rssJson = appObj.getNzbRssJson();

                        // Create a new panel to add everything to
                        NzbDataTabbedPanel tabPane = new NzbDataTabbedPanel(show.toString());

                        //The following line enables to use scrolling tabs.
                        tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

                        // Create the list
                        MyNzbRssList rssList = new MyNzbRssList(myepsJson, rssJson, show);
                        DefaultListModel model = (DefaultListModel)rssList.getModel();
                        model.addElement("Loading...");

                        // Create the panel
                        JScrollPane panel = new JScrollPane();
                        panel.setViewportView(rssList);

                        for(NewzNabJson json : appObj.getNewzNabServerList()) 
                        {
                            // Check to see if it is enabled
                            if(json.isEnabled())
                            {
                                MyNewzNabList list = new MyNewzNabList(myepsJson, json, show);
                                model = (DefaultListModel)list.getModel();
                                model.addElement("Loading...");

                                // Create the panel
                                panel = new JScrollPane();
                                panel.setViewportView(list);

                                // Icon file
                                String icon = json.getIconFileLocation();

                                // Check for the file
                                File f = new File(icon);

                                // If it isn't there we search for the file in the directory
                                if(!f.exists() && !f.isDirectory())
                                {
                                    icon =  "/icons/null.png";
                                    json.setIconFileLocation(icon);
                                }

                                // Add it to a new tab
                                tabPane.addTab(json.getName(), new ImageIcon(icon), panel, "NewzNab API");

                                // Call the data
                                Thread thread = new Thread(new GetNewzNabData(list));
                                thread.start();
                            }
                        }
                        
                        // Add to the tab pane
                        tabPane.addTab(rssJson.getName(), new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/nzb-rss.png"))), panel, "NewzNab API");

                        // Call the data
                        Thread thread = new Thread(new GetNzbRssData(rssList));
                        thread.start();

                        // Set the view
                        parent.getTabbedPane().setViewportView(tabPane);
                    }

                    // Right Click && not popped up
                    if(SwingUtilities.isRightMouseButton(e) && !e.isPopupTrigger())
                    {
                        // The popup menu
                        JPopupMenu popup = new JPopupMenu();
                        
                        // Setup the action
                        ActionListener action = new ActionListener()
                        {
                            @Override
                            public void actionPerformed(ActionEvent e)
                            {
                                Thread thread = new Thread(new ClickAction(myepjson, show, e.getActionCommand()));
                                thread.start();
                            }
                        };

                        if(!show.isAquired())
                        {
                            // Aquired
                            JMenuItem menuItem = new JMenuItem("Set Aquired");
                            menuItem.setActionCommand("aquired");
                            menuItem.addActionListener(action);
                            popup.add(menuItem);
                        }

                        if(!show.isSeen())
                        {
                            // Seen
                            JMenuItem menuItem = new JMenuItem("Set Seen");
                            menuItem.setActionCommand("seen");
                            menuItem.addActionListener(action);
                            popup.add(menuItem);
                        }

                        // Show the menu
                        popup.show((JComponent) e.getSource(), e.getX(), e.getY());
                    }
                }
                
                else if(node.getUserObject() instanceof TvMazeJson)
                {
                    // Right Click
                    if(SwingUtilities.isRightMouseButton(e) && !e.isPopupTrigger())
                    {
                        // Get the show data
                        TvMazeJson json = (TvMazeJson)node.getUserObject();

                        // The popup menu
                        JPopupMenu popup = new JPopupMenu();

                        // Add the link to the webpage
                        JMenuItem menuItem = new JMenuItem("View " + json.getName() + " at TvMaze?");
                        menuItem.setActionCommand("webpage");
                        menuItem.addActionListener(new WebPageConnectionActionListener(json.getUrl()));
                        popup.add(menuItem);
                        
                        // Get the child->child
                        Show show = (Show)((DefaultMutableTreeNode)node.getChildAt(0).getChildAt(0)).getUserObject();
                        
                        // Split up the guid
                        String[] split = show.getGuid().split("-");

                        // Develop the link to the series
                        String link = myepjson.getAddress() + "show/id-" + split[0];
                        
                        // Add the link to the webpage
                        menuItem = new JMenuItem("View " + show.getSeriesName() + " at MyEpisodes?");
                        menuItem.setActionCommand("webpage");
                        menuItem.addActionListener(new WebPageConnectionActionListener(link));
                        popup.add(menuItem);

                        // Show the menu
                        popup.show((JComponent) e.getSource(), e.getX(), e.getY());
                    }
                }
            }
        }); 
    }
    
    /**
     * Over ridden getToolTipText
     * @param ev is the mouse event
     * @return String
    */
    @Override
    public String getToolTipText(MouseEvent ev)
    {
	if(getRowForLocation(ev.getX(), ev.getY()) == -1)
	    return null;
	
	TreePath curPath = getPathForLocation(ev.getX(), ev.getY());
	DefaultMutableTreeNode node = (DefaultMutableTreeNode)curPath.getLastPathComponent();
	
	if(node != null)
            return ((Show)node.getUserObject()).getToolTip();
	
	return null;
    }
    
    /**
     * Expands the whole tree for me
     * @param parent is the parent node
    */
    public void expandAll(TreePath parent) 
    {
	TreeNode node = (TreeNode)this.getModel().getRoot();
	if(node.getChildCount() >= 0) 
	{
	    for(Enumeration e = node.children(); e.hasMoreElements();) 
	    {
		TreeNode n = (TreeNode) e.nextElement();
		TreePath path = parent.pathByAddingChild(n);
		expandAll(path);
	    }
	}
	
	this.expandPath(parent);
    }
    
    /**
     * Returns the application json object to caller
     * @return 
     */
    public ApplicationJson getAppJsonObject()
    {
        return appObj;
    }
    
    /**
     * Returns the myepisodes json object to caller
     * @return 
     */
    public MyEpisodesJson getMyEpsJsonObject()
    {
        return myepjson;
    }
     
    /**
     * Returns the tabbed pane
     * @return 
     */
    public JScrollPane getTabbedPane()
    {
        return tabbedPane;
    }
}

class WebPageConnectionActionListener implements ActionListener
{
    private String link = null;
    
    public WebPageConnectionActionListener(String l)
    {
        super();
        link = l;
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        // Log this link
        LogManager.getLogger("info").info(link);
                        
        // Open up a request to set the show as being seen and or watched
        // Open the webpage
        if(Desktop.isDesktopSupported())
        {
            Desktop desktop = Desktop.getDesktop();
            try
            {
                desktop.browse(new URI(link));
            }
            catch(IOException | URISyntaxException ex) 
            {
                LogManager.getLogger().error(WebPageConnectionActionListener.class.getName(), ex);
            }
        }

        else
        {
            Runtime runtime = Runtime.getRuntime();
            try
            {
                runtime.exec("xdg-open " + link);
            }
            catch(IOException ex) 
            {
                LogManager.getLogger().error(WebPageConnectionActionListener.class.getName(), ex);
            }
        }
    }
}