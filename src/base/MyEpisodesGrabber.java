/********************************************************************************************
 * MyEpisodesGrabber.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 05, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * - Add search feature for movies etc
 * - Implement getting nzbs via the tor network
 * - Implement getting NNTP via VPN without PIA
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * v1.00
 *  - Changed toolbar icons from external to internal
 * 
********************************************************************************************/

package base;

// Local imports
import configs.*;
import myEpisodesData.*;

// Imports
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import com.google.gson.Gson;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.apache.logging.log4j.LogManager;

public class MyEpisodesGrabber extends JFrame
{
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        try
        {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        }
        catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
        {
            LogManager.getLogger().error(MyEpisodesGrabber.class.getName(), ex);
        }
        
	/* Create and display the form */
	java.awt.EventQueue.invokeLater(() -> 
        {
            new MyEpisodesGrabber().setVisible(true);
        });
    }
    
    ///////////////////////////////////////////////////////////////////////////
    //          VARIABLES
    ///////////////////////////////////////////////////////////////////////////
    private ApplicationJson applicationJson = null;
    private MyEpisodesTree myTreeMyEpisodes = null;

    ///////////////////////////////////////////////////////////////////////////
    //          CONSTRUCTOR
    ////////////////////////////////////////////////////////////////////////////
    public MyEpisodesGrabber()
    {
        // Call super
        super();
        
        // Set the icon
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/myeps.png")));
        
        // Initialize the components
	initComponents();
        
        //*****************************************************************************************************************************
        //*              LOAD APPLICATION JSON START
        //*****************************************************************************************************************************
        
        // Initialize the the my eps url
	applicationJson = new ApplicationJson();
        Gson gson = new Gson();
        try 
        {
            applicationJson = gson.fromJson(new FileReader("config.json"), ApplicationJson.class);
        }
        catch(FileNotFoundException ex) 
        {
            // Not worried as it will create the file
            //LogManager.getLogger().error(MyEpisodesGrabber.class.getName(), ex);
        }
        
        //*****************************************************************************************************************************
        //*              LOAD APPLICATION JSON END
        //*****************************************************************************************************************************
       
        //*****************************************************************************************************************************
        //*              SETUP THE MENU FOR LOOK AND FEEL START
        //*****************************************************************************************************************************
        
        // Setup the menu for the look and feel
        ButtonGroup group = new ButtonGroup();
        for(UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
        {
            JCheckBoxMenuItem item = new JCheckBoxMenuItem(info.getName(), ((applicationJson.getApplicationSettings("lookandfeel").equals(info.getName())) ? true : false));
            item.setHorizontalTextPosition(JMenuItem.RIGHT);
            item.addActionListener(new ActionListener() 
            {
                public void actionPerformed(ActionEvent e) 
                {
                    applicationJson.setApplicationSettings("lookandfeel", e.getActionCommand());
                    changeLookAndFeel(e.getActionCommand());
                }
            });
            group.add(item);
            jMenuLookAndFeel.add(item);
        }
        
        // Change look and feel
        changeLookAndFeel((String)applicationJson.getApplicationSettings("lookandfeel"));
        
        //*****************************************************************************************************************************
        //*              SETUP THE MENU FOR LOOK AND FEEL END
        //*****************************************************************************************************************************
        //*****************************************************************************************************************************
        //*              TOOLBAR BUTTONS START
        //*****************************************************************************************************************************
        // Create the MyEps button
        ImageIcon imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/myeps.png")).getScaledInstance(32, 32, Image.SCALE_SMOOTH));
        JButton myEpsButton = new JButton(imageIcon);
        myEpsButton.setToolTipText("Refresh My Episodes");
        myEpsButton.setActionCommand("reload");
        
        ActionListener reload = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event) 
            {
                // Refresh the MyEps
                DefaultTreeModel model = (DefaultTreeModel)myTreeMyEpisodes.getModel();
                model.setRoot(new DefaultMutableTreeNode("Loading..."));
                model.reload();

                // Call the data
                Thread thread = new Thread(new GetMyEpisodesData(myTreeMyEpisodes, applicationJson));
                thread.start();
            }
        };
        
        myEpsButton.addActionListener(reload);
        jMenuItemReload.addActionListener(reload);
        
        // Create the MyEps button
        imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/calendar.png")).getScaledInstance(32, 32, Image.SCALE_SMOOTH));
        JButton myEpsWebpage = new JButton(imageIcon);
        myEpsWebpage.setToolTipText("Open My Episodes Calendar");
        myEpsWebpage.setActionCommand("calendar");
        
        ActionListener calendar = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event) 
            {
                // Open the webpage
                if(Desktop.isDesktopSupported())
                {
                    Desktop desktop = Desktop.getDesktop();
                    try
                    {
                        desktop.browse(new URI(applicationJson.getMyEpisodes().getAddress() + "cal/"));
                    }
                    catch(IOException | URISyntaxException ex) 
                    {
                        LogManager.getLogger().error(MyEpisodesGrabber.class.getName(), ex);
                    }
                }

                else
                {
                    Runtime runtime = Runtime.getRuntime();
                    try
                    {
                        runtime.exec("xdg-open " + applicationJson.getMyEpisodes().getAddress());
                    }
                    catch(IOException ex) 
                    {
                        LogManager.getLogger().error(MyEpisodesGrabber.class.getName(), ex);
                    }
                }
            }
        };
        
        myEpsWebpage.addActionListener(calendar);
        jMenuItemCalendar.addActionListener(calendar);
        
        // Make a the config button
        MyEpisodesGrabber temp = this;
        imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/config.png")).getScaledInstance(32, 32, Image.SCALE_SMOOTH));
        JButton mySettingsButton = new JButton(imageIcon);
        mySettingsButton.setToolTipText("Open Settings Dialog");
        mySettingsButton.setActionCommand("settings");
        
        ActionListener settings = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                // Create the Config button
                ConfigDialog frame = new ConfigDialog(temp, applicationJson);
                frame.setVisible(true);
            }
        };
        
        mySettingsButton.addActionListener(settings);
        jMenuItemSettings.addActionListener(settings);
        
        // Make a the log button
        imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/log.png")).getScaledInstance(32, 32, Image.SCALE_SMOOTH));
        JButton myLogButton = new JButton(imageIcon);
        myLogButton.setToolTipText("Open Log Dialog");
        myLogButton.setActionCommand("log");
        
        ActionListener log = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                // Create the Config button
                ViewDebugDialog frame = new ViewDebugDialog(temp);
                frame.setVisible(true);
            }
        };
        
        myLogButton.addActionListener(log);
        jMenuItemLog.addActionListener(log);
        
        // Now add the buttons to the ribbon
        jToolBar.add(myEpsButton);
        jToolBar.add(myEpsWebpage);
        jToolBar.add(mySettingsButton);
        jToolBar.add(myLogButton);
        
        //*****************************************************************************************************************************
        //*              TOOLBAR BUTTONS END
        //*****************************************************************************************************************************
        
        // Initialize the myeps tree
        myTreeMyEpisodes = new MyEpisodesTree(applicationJson, jPanelForTabs);
        
        DefaultTreeModel model = (DefaultTreeModel)myTreeMyEpisodes.getModel();
        model.setRoot(new DefaultMutableTreeNode("Loading..."));
        model.reload();
        
        // Call the data
        Thread thread = new Thread(new GetMyEpisodesData(myTreeMyEpisodes, applicationJson));
        thread.start();
        
        // Add to the screen
        jScrollPaneMyEpisodes.setViewportView(myTreeMyEpisodes);
        
        //*****************************************************************************************************************************
        //*              MY EPISODES TREE END
        //*****************************************************************************************************************************
    }
    
    /**
     * Method to change the look and feel to a different version
     * @param towhat 
    */
    public void changeLookAndFeel(String towhat)
    {
	try
	{
	    for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
	    {
		if(towhat.equals(info.getName()))
		{
		    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    SwingUtilities.updateComponentTreeUI(this);
                    this.pack();
		    break;
		}
	    }
	}
	catch(ClassNotFoundException|InstantiationException|IllegalAccessException|javax.swing.UnsupportedLookAndFeelException ex)
	{
	    LogManager.getLogger().error(MyEpisodesGrabber.class.getName(), ex);
	}
    }
    
    /**
	This area is done by the application
    */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jScrollPaneMyEpisodes = new javax.swing.JScrollPane();
        jToolBar = new javax.swing.JToolBar();
        jPanelForTabs = new javax.swing.JScrollPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemReload = new javax.swing.JMenuItem();
        jMenuItemCalendar = new javax.swing.JMenuItem();
        jMenuItemSettings = new javax.swing.JMenuItem();
        jMenuItemLog = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuLookAndFeel = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPaneMyEpisodes.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        jToolBar.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        jToolBar.setFloatable(false);
        jToolBar.setRollover(true);

        jPanelForTabs.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        jMenu1.setText("File");

        jMenuItemReload.setText("Reload My Episodes");
        jMenuItemReload.setActionCommand("reload");
        jMenu1.add(jMenuItemReload);

        jMenuItemCalendar.setText("My Episodes Calendar");
        jMenuItemCalendar.setActionCommand("calendar");
        jMenu1.add(jMenuItemCalendar);

        jMenuItemSettings.setText("Settings");
        jMenuItemSettings.setActionCommand("settings");
        jMenu1.add(jMenuItemSettings);

        jMenuItemLog.setText("Log");
        jMenuItemLog.setActionCommand("log");
        jMenu1.add(jMenuItemLog);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenuLookAndFeel.setText("Look & Feel");
        jMenu2.add(jMenuLookAndFeel);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPaneMyEpisodes, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanelForTabs, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPaneMyEpisodes, javax.swing.GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE)
                    .addComponent(jPanelForTabs))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItemCalendar;
    private javax.swing.JMenuItem jMenuItemLog;
    private javax.swing.JMenuItem jMenuItemReload;
    private javax.swing.JMenuItem jMenuItemSettings;
    private javax.swing.JMenu jMenuLookAndFeel;
    private javax.swing.JScrollPane jPanelForTabs;
    private javax.swing.JScrollPane jScrollPaneMyEpisodes;
    private javax.swing.JToolBar jToolBar;
    // End of variables declaration//GEN-END:variables

}
