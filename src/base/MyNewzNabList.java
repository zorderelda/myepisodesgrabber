/********************************************************************************************
 * MyNewzNabList.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 06, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package base;

import configs.MyEpisodesJson;
import configs.NewzNabJson;
import myEpisodesData.Show;

import java.awt.Desktop;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;

public class MyNewzNabList extends JList
{
    private MyEpisodesJson myEpsJson = null;
    private NewzNabJson nnjson = null;
    private Show show = null; 
    
    public MyNewzNabList(MyEpisodesJson m, NewzNabJson n, Show s)
    {
	// Call to super
	super(new DefaultListModel());
        
        // Set the app object
        myEpsJson = m;
        
        // Set the nnab
        nnjson = n;
        
        // Set the Show
        show = s;
        
        // Set only to single selection
        this.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        
        // Add a mouse listener
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e) 
            {
                MyNewzNabList list = (MyNewzNabList)e.getSource();
                int index = list.locationToIndex(e.getPoint());
                
                if(index >= 0) 
                {
                    if(SwingUtilities.isLeftMouseButton(e))
                    {
                        if(e.getClickCount() == 1)
                        {
                            if(list.getModel().getElementAt(index) instanceof Nzb)
                            {
                                // We need to get the compiled get link and the shows guid and save name
                                NewzNabJson nnjson = list.getNNJson();
                                Show show = list.getShow();
                                Nzb nzb = (Nzb)list.getModel().getElementAt(index);

                                // Download the data with a thread
                                Thread t = new Thread()
                                {
                                    @Override
                                    public void run()
                                    {
                                        JFileChooser c = new JFileChooser();
                                        c.setSelectedFile(new File(System.getProperty("user.home") + "/Downloads/" + show.getSaveName()));
                                        if(c.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) 
                                        {
                                            try 
                                            {
                                                FileUtils.copyURLToFile(new URL(nnjson.getCompiledGetURL().replaceAll("%SHOWID%", nzb.getGuid())), c.getSelectedFile());
                                                JOptionPane.showMessageDialog(null, "Downloaded: " + show.getSaveName(), "Downloaded", JOptionPane.INFORMATION_MESSAGE);
                                            }
                                            catch(IOException ex) 
                                            {
                                                LogManager.getLogger().error(MyNewzNabList.class.getName(), ex);
                                                JOptionPane.showMessageDialog(null, "FAILED to Download: " + show.getSaveName(), "Downloaded", JOptionPane.ERROR_MESSAGE);
                                            }
                                        }
                                    }
                                };

                                // Run the thread
                                t.start();
                            }
                        }
                    }
                    
                    else if(SwingUtilities.isRightMouseButton(e))
                    {
                        if(e.getClickCount() == 1)
                        { 
                            Nzb nzb = (Nzb)list.getModel().getElementAt(index);
                            
                            if(Desktop.isDesktopSupported())
                            {
                                Desktop desktop = Desktop.getDesktop();
                                try
                                {
                                    desktop.browse(new URI(nzb.getComments()));
                                }
                                catch(IOException | URISyntaxException ex) 
                                {
                                    LogManager.getLogger().error(MyNewzNabList.class.getName(), ex);
                                }
                            }
                            
                            else
                            {
                                Runtime runtime = Runtime.getRuntime();
                                try
                                {
                                    runtime.exec("xdg-open " + nzb.getComments());
                                }
                                catch(IOException ex) 
                                {
                                    LogManager.getLogger().error(MyNewzNabList.class.getName(), ex);
                                }
                            }
                        }
                    }
                }
            }
        });
    }
    
    /**
     * Gets the json associated with this list
     * @return 
     */
    public MyEpisodesJson getMyEpsObject()
    {
        return myEpsJson;
    }
    
    /**
     * Gets the json associated with this list
     * @return 
     */
    public NewzNabJson getNNJson()
    {
        return nnjson;
    }
    
    /**
     * Gets the show associated with this list
     * @return 
     */
    public Show getShow()
    {
        return show;
    }
    
    /**
	Over ridden getToolTipText
	@param event is the mouse event
	@return String
    */
    @Override
    public String getToolTipText(MouseEvent event)
    {
        Point p = event.getPoint();
        int location = locationToIndex(p);
        return ((Nzb)getModel().getElementAt(location)).getToolTip();
    }
}

