/********************************************************************************************
 * ClickActionListener.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 05, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * v1.02.*
 * - Changed from an action listener to a runnable to remove lag on the popup
 * 
********************************************************************************************/

package base;

import configs.MyEpisodesJson;
import myEpisodesData.Show;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ClickAction implements Runnable
{
    private MyEpisodesJson myepjson = null;
    private Show show = null;
    private String action = null;
    
    public ClickAction(MyEpisodesJson j, Show s, String a)
    {
        super();
        myepjson = j;
        show = s;
        action = a;
    }

    @Override
    public void run()
    {
        // The login page
        String loginLink = myepjson.getAddress() + "login.php";
        
        // The show page
        String showLink = myepjson.getAddress() + "myshows.php";
        
        // Split up the guid
        String[] split = show.getGuid().split("-");
        
        try
        {
            // Initial Connection
            Response initial = Jsoup.connect(loginLink)
                    .referrer(myepjson.getAddress())
                    .userAgent("Mozilla/5.0")
                    .timeout(10 * 1000)
                    .followRedirects(true)
                    .method(Method.GET)
                    .execute();
            
            // Now login
            Document doc = Jsoup.connect(loginLink)
                    .referrer(loginLink)
                    .userAgent("Mozilla/5.0")
                    .data("action", "Login")
                    .data("username", myepjson.getUserName())
                    .data("password", myepjson.getPassword())
                    .data("u", "")
                    .timeout(10 * 1000)
                    .followRedirects(true)
                    .cookies(initial.cookies())
                    .post();
            
            // Log the link sent
            LogManager.getLogger("info").info(initial.cookies().toString());
            
            String link = showLink + "?action=Update&showid=" + split[0] + "&season=" + split[1] + "&episode=" + split[2] + "&" + action + "=1";
            
            // Log the link sent
            LogManager.getLogger("info").info(link);
            
            // http://www.myepisodes.com/myshows.php?action=Update&showid=10408&season=5&episode=9&seen=1
            Response initia2l = Jsoup.connect(link)
                    .referrer(showLink)
                    .userAgent("Mozilla/5.0")
                    .timeout(10 * 1000)
                    .followRedirects(true)
                    .cookies(initial.cookies())
                    .method(Method.GET)
                    .execute();
            
            // Only set seen if seen
            if(action.equals("seen"))
                show.setSeen();
            
            // Its been aquired
            show.setAquired();
        }
        catch(IOException ex)
        {
            // Log the data
            LogManager.getLogger().error(ClickAction.class.getName(), ex);
        }
    }
}
