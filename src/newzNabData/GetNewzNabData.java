/********************************************************************************************
 * GetNewzNabData.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 06, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package newzNabData;

import base.Nzb;
import base.MyNewzNabList;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.regex.*;
import javax.swing.DefaultListModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class GetNewzNabData implements Runnable
{
    private URL link = null;
    private volatile MyNewzNabList syncList = null;
    
    public GetNewzNabData(MyNewzNabList list)
    {
        syncList = list;
                
        // Create the search url
        String searchString = list.getNNJson().getCompiledSearchURL();
                       
        try 
        {
            // Id type
            String type = "q";
        
            // Find out what we are using
            // Regex stuff
            String pattern = "([a-zA-Z]+)=";

            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(list.getNNJson().getSearchURL());

            if(m.find())
            {
                // Sanity check
                switch(m.group(1))
                {
                    case "tvmazeid":
                    case "rid":
                    case "tvdbid":
                    case "imdbid":
                        type = m.group(1);
                        break;
                    default:
                        searchString = searchString.replaceFirst("\\&([a-zA-Z]+)=", "\\&q=");
                        break;
                }
            }

            // Init the series
            String series = (type.equals("q")) ? URLEncoder.encode(StringEscapeUtils.escapeHtml4(list.getShow().getSeriesName()), "UTF-8") : list.getShow().getVar(type);

            // Get the link
            searchString = searchString
                    .replaceAll("%SERIES%", series)
                    .replaceAll("%SEASON%", list.getShow().getSeasonNumber())
                    .replaceAll("%EPISODE%", list.getShow().getEpisodeNumber());
        }
        catch(UnsupportedEncodingException | NullPointerException ex) 
        {
            LogManager.getLogger().error(GetNewzNabData.class.getName(), ex);
        }

        // Log the data
        LogManager.getLogger("info").info(searchString);
        
        try 
        {
            link = new URL(searchString);
        }
        catch(MalformedURLException ex) 
        {
            LogManager.getLogger().error(GetNewzNabData.class.getName(), ex);
        }
    }
    
    @Override
    public void run() 
    {
        File tempFile = null;
        try 
        {
            synchronized(syncList)
            {
                tempFile = File.createTempFile(syncList.getNNJson().getName(), ".temp");
                tempFile.deleteOnExit();
                FileUtils.copyURLToFile(link, tempFile); 
            }
        }
        catch(IOException ex) 
        {
            LogManager.getLogger().error(GetNewzNabData.class.getName(), ex);
        }
        
        // Create a new model
        DefaultListModel model = new DefaultListModel();
	
	try 
        {
            SAXReader reader = new SAXReader();
            Document document = reader.read(tempFile);
            List<Node> nodes = document.selectNodes("/rss/channel/item");
            
            for(Node node : nodes)
            {
                if(node.getName().equalsIgnoreCase("item"))
                {
                    // Add the nzb to the top level and keep the next level to add to
                    model.addElement(new Nzb(node));
                }
            }
        }
        catch(DocumentException ex) 
        {
            LogManager.getLogger().error(GetNewzNabData.class.getName(), ex);
        }
        
        synchronized(syncList)
        {
            if(model.getSize() < 1)
                model.addElement("Empty...");
            
            syncList.setModel(model);
        }
    }
}
