/********************************************************************************************
 * TvMazeApiJson.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Dec 15, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * 1.00
 * - Added seperator and replacement variables to fix problems with : etc
 * 
********************************************************************************************/

package configs;

public class TvMazeApiJson 
{
    private String address = null;
    private String seperator = null;
    private String replacement = null;
    
    public TvMazeApiJson()
    {
        address = "http://api.tvmaze.com/singlesearch/shows?q=%SERIES%";
        seperator = "-";
        replacement = "\\.\\:";
    }
    
    // Setters
    public void setAddress(String u) { address = u; }
    public void setSeparator(String u) { seperator = u; }
    public void setReplacement(String u) { replacement = u; }
    
    // Getters
    public String getAddress() { return address; }
    public String getSeparator() { return seperator; }
    public String getReplacement() { return replacement; }
}
