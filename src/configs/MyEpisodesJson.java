/********************************************************************************************
 * MyEpisodes.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Oct 23, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * 
********************************************************************************************/

package configs;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.logging.log4j.LogManager;

public class MyEpisodesJson
{
    private String address;
    private String page;
    private String username;
    private String password;
    private String replacements;
    
    public MyEpisodesJson()
    {
        address = "http://www.myepisodes.com/";
        page = "rss.php?feed=unacquired&showignored=0&uid=%USERNAME%&pwdmd5=%MD5%";
        username = "";
        password = "";
        replacements = "\\/:*?\"|";
    }
    
    // Setters
    public void setAddress(String u) { address = u; }
    public void setPage(String s) { page = s; }
    public void setUserName(String s) { username = s; }
    public void setPassword(String s) { password = s; }
    public void setReplacement(String s) { replacements = s; }
    
    // Getters
    public String getAddress() { return address; }
    public String getPage() { return page; }
    public String getUserName() { return username; }
    public String getMd5Password() { return md5(password); }
    public String getPassword() { return password; }
    public String getReplacement() { return replacements; }
    
    // To String
    public String getCompiledAddress()
    {
        // Setup the string to return
        String compiled = address;
        
        // Check that the last / is there
        if(compiled.charAt(compiled.length() - 1) != '/')
            compiled += "/";
        
        return compiled + page
                .replaceAll("%USERNAME%", username)
                .replaceAll("%MD5%", this.getMd5Password());
    }
    
    /***
     * Sets up the MD5 password
     * @param md5
     * @return 
     */
    private String md5(String input) 
    {	
        String md5 = password;

        if(null == input)
            return null;
        
        try
        {   
            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());
            
            //Converts message digest value in base 16 (hex) 
            md5 = new BigInteger(1, digest.digest()).toString(16);
        }
        catch(NoSuchAlgorithmException ex)
        {
            LogManager.getLogger().error(MyEpisodesJson.class.getName(), ex);
        }
        
        return md5;
    }
}
