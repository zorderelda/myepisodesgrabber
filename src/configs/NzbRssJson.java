/********************************************************************************************
 * NzbRssJson.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 10, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package configs;

public class NzbRssJson 
{
    private String nzbRssLink;
    private String nzbRssView;
    private String nzbRssSeparator;
    private String replacement;
    
    public NzbRssJson()
    { 
        // NZB-RSS get rss data
        nzbRssLink = "https://www.nzb-rss.com/rss/%SERIESNAME%.rss";
        
        // NZB-RSS view show data
        nzbRssView = "https://www.nzb-rss.com/show/%SERIESNAME%";
        
        // Separator
        nzbRssSeparator = "_";
        
        // Replacement regex
        replacement = "\\.";
    }
    
    public String getName()
    {
        return "Nzb-Rss";
    }
    
    public String getLink() 
    { 
        return nzbRssLink; 
    }
    
    public String getViewLink() 
    { 
        return nzbRssView; 
    }
    
    public String getSeparator() 
    { 
        return nzbRssSeparator; 
    }
    
    public String getReplacement() 
    { 
        return replacement; 
    }
    
    public void setLink(String s) 
    { 
        nzbRssLink = s; 
    }
    
    public void setViewLink(String s) 
    { 
        nzbRssView = s; 
    }
    
    public void setSeparator(String s) 
    { 
        nzbRssSeparator = s; 
    }
    
    public void setReplacement(String s) 
    { 
        replacement = s; 
    }
}
