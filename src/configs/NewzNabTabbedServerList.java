/********************************************************************************************
 * NewzNabTabbedServerList.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 01, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package configs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class NewzNabTabbedServerList extends JTabbedPane
{
    // The application object
    private ApplicationJson appObj;
    private int oldIndex;
    
    public NewzNabTabbedServerList(ApplicationJson obj)
    {
        // JTabbedPane constructor
        super();
        
        appObj = obj;
        oldIndex = 1;
        
        JPopupMenu contextMenu = new JPopupMenu("Edit");
        JMenuItem delete = new JMenuItem("Delete Server");
        delete.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                NewzNabTabbedServerList temp = (NewzNabTabbedServerList)((JPopupMenu)((JMenuItem)e.getSource()).getParent()).getInvoker();
                NewzNabJson nn = ((NewzNabConfig)temp.getSelectedComponent()).getNNJson();
                temp.remove(temp.getSelectedIndex());
                appObj.removeNewNabServer(nn);
            }
        });
        
        JMenuItem change = new JMenuItem("Change Name");
        change.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                String name = JOptionPane.showInputDialog(null, "What's the servers new name?", "Rename", JOptionPane.QUESTION_MESSAGE);
                if(name != null && name.length() > 0)
                {
                    NewzNabTabbedServerList temp = (NewzNabTabbedServerList)((JPopupMenu)((JMenuItem)e.getSource()).getParent()).getInvoker();
                    temp.setTitleAt(temp.getSelectedIndex(), name);
                    NewzNabJson nn = ((NewzNabConfig)temp.getSelectedComponent()).getNNJson();
                    nn.setName(name);
                }
            }
        });
        
        contextMenu.add(delete);
        contextMenu.add(change);
        setComponentPopupMenu(contextMenu);
        
        // Add the + key
        this.addTab(" +  ", null, null, "Add a Server");  
        
        // Loop through and get the tabs
        for(NewzNabJson json : appObj.getNewzNabServerList()) 
        {   
            NewzNabConfig panel = new NewzNabConfig(json);
            panel.setComponentPopupMenu(contextMenu);
            
            // Icon file
            String icon = json.getIconFileLocation();
            
            // Add the tab
            this.addTab(json.getName(), icon.contains("null.png") ? null : new ImageIcon(icon) , panel, "NewzNab API");
        }
        
        // Set to first one
        this.setSelectedIndex(oldIndex);
        
        // Add a change listener
        this.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent e) 
            {
                JTabbedPane parent = (JTabbedPane)e.getSource();
                
                if(parent.getSelectedIndex() == 0)
                {
                    NewzNabJson json = obj.addNewzNabServer("New");
                    parent.insertTab(json.getName(), null, new NewzNabConfig(json), "NewzNab API", parent.getTabCount());
                    parent.setSelectedIndex(parent.getTabCount() - 1);
                    return;
                }
                
                oldIndex = parent.getSelectedIndex();
            }
        });
    }
}
