/********************************************************************************************
 * NewzNabJson.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Oct 23, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

package configs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewzNabJson 
{
    private String name;
    private boolean enabled;
    private String key;
    private String base;
    private String search;
    private String get;
    private String caps;
    private String ids;
    private String icon;
    
    // Setup the replacement variables
    private String sApiKeyReplacement = "%APIKEY%";
    private String gApiKeyReplacement = "%APIKEY%";
    
    // Constructor
    public NewzNabJson() 
    {
        name = "";
        enabled = false;
        key = "";
        base = "https://www.nothing.net/api?t=";
        search = "tvsearch&q=%SERIES%&season=%SEASON%&ep=%EPISODE%&apikey=%APIKEY%";
        get = "get&id=%SHOWID%&apikey=%APIKEY%";
        caps = "caps";
        ids = "";
        icon = "/icons/null.png";
        
        String regex = "&apikey=(\\%[a-zA-Z0-9]+\\%)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(search);

        if(matcher.find()) 
        {
            sApiKeyReplacement = matcher.group(1);
        }
        
        matcher = pattern.matcher(get);

        if(matcher.find()) 
        {
            gApiKeyReplacement = matcher.group(1);
        }
    }
    
    public NewzNabJson(String n)
    {
        this();
        name = n;
    }
    
    // Getters
    public String getName() 
    { 
        return name; 
    }
    
    public boolean isEnabled() 
    { 
        return enabled; 
    }
    
    public String getKey() 
    { 
        return key; 
    }
    
    public String getBaseURL() 
    { 
        return base; 
    }
    
    public String getSearchURL() 
    { 
        return search; 
    }
    
    public String getCompiledSearchURL() 
    { 
        return base + search.replaceAll(sApiKeyReplacement, getKey()); 
    }
    public String getGetURL() 
    { 
        return get; 
    }
    
    public String getCompiledGetURL() 
    { 
        return base + get.replaceAll(gApiKeyReplacement, getKey()); 
    }
    
    public String getCapsURL() 
    { 
        return caps; 
    }
    
    public String getCompiledCapsURL() 
    { 
        return base + caps; 
    }
    
    public String getIds() 
    { 
        return ids; 
    }
    
    public String getIconFileLocation() 
    { 
        return icon; 
    }
    
    // Setters
    public void setName(String s) 
    { 
        name = s; 
    }
    
    public void setEnabled(boolean b) 
    { 
        enabled = b; 
    }
    
    public void setKey(String s) 
    { 
        key = s; 
    }
    
    public void setBaseURL(String u) 
    { 
        base = u; 
    }
    
    public void setSearchURL(String u) 
    {
        search = u; 
    }
    
    public void setGetURL(String u) 
    { 
        get = u; 
    }
    
    public void setCapsURL(String u) 
    { 
        caps = u; 
    }
    
    public void setIds(String u) 
    { 
        ids = u; 
    }
    
    public void setIconFileLocation(String s) 
    { 
        icon = s; 
    }
    
    @Override
    public String toString() 
    { 
        return name; 
    }
}
