/********************************************************************************************
 * ApplicationJson.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Oct 23, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * v1.00
 * - Removed the debug option
 * 
********************************************************************************************/

package configs;

import java.util.ArrayList;
import java.util.HashMap;

public class ApplicationJson 
{
    private HashMap<String, Object> application = new HashMap<>();
    private MyEpisodesJson myepisodes = new MyEpisodesJson();
    private ArrayList<NewzNabJson> nnservers = new ArrayList<>();
    private TvMazeApiJson tvmaze = new TvMazeApiJson();
    private NzbRssJson nzbRss = new NzbRssJson();
    
    public ApplicationJson()
    {
        // Application
        application.put("lookandfeel", "windows");
        
        // MyEpisodes
        myepisodes.setUserName("");
        myepisodes.setPassword("");
        
        // Newznab Servers
        nnservers.add(new NewzNabJson("temp"));
        
        // TvMaze API and Nzbrss will init their own
    }
    
    // Getters
    public Object getApplicationSettings(String key)
    {
        return application.get(key); 
    }
    
    public MyEpisodesJson getMyEpisodes() 
    { 
        return myepisodes; 
    }
    
    public TvMazeApiJson getTvMazeApiJson() 
    { 
        return tvmaze; 
    }
    
    public ArrayList<NewzNabJson> getNewzNabServerList() 
    { 
        return nnservers; 
    }
    
    public NzbRssJson getNzbRssJson()
    {
        return nzbRss;
    }
    
    public void setApplicationSettings(String key, Object obj)
    {
        application.put(key, obj);
    }
    
    public NewzNabJson addNewzNabServer(String name)
    {
        // Create new server
        NewzNabJson nnab = new NewzNabJson(name);
        
        // Add it to the Newznab Servers List
        nnservers.add(nnab);
        
        // Return the new server
        return nnservers.get(nnservers.indexOf(nnab));
    }
    
    public void removeNewNabServer(NewzNabJson nn)
    {
        nnservers.remove(nnservers.indexOf(nn));
    }
}