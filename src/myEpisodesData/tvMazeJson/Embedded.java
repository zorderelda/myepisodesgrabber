package myEpisodesData.tvMazeJson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Embedded
{
    @SerializedName("episodes")
    @Expose
    private List<Episode> episodes = null;

    /**
     *
     * @return
     * The episodes
     */
    public List<Episode> getEpisodes()
    {
        return episodes;
    }

    /**
     *
     * @param episodes
     * The episodes
     */
    public void setEpisodes(List<Episode> episodes)
    {
        this.episodes = episodes;
    }
}
