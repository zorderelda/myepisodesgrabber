/********************************************************************************************
 * Show.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 07, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * -
 * 
********************************************************************************************/

// Package
package myEpisodesData;

import java.util.*;
import java.util.regex.*;
import org.dom4j.*;

public class Show 
{
    private HashMap<String, String> vars = new HashMap<>();
    private Object newzNabTree = new Object();
    private boolean isAquired = false;
    private boolean isSeen = false;
    
    public Show(Node node, String replacement)
    {
        // Place the replacements
        vars.put("replace", replacement);
        
        // Keep the full data for later
        vars.put("title", node.selectSingleNode("title").getText());
        vars.put("guid", node.selectSingleNode("guid").getText());
        
	// Regex stuff
        String pattern = "\\[\\s([\\x00-\\x7F]+)\\s\\]\\[\\s([0-9]+)x([0-9]+)\\s\\]\\[\\s([\\x00-\\x7F]+)\\s\\]\\[\\s([0-9]{2}-\\w+-[0-9]{4})\\s\\]";
	
	Pattern r = Pattern.compile(pattern);
	Matcher m = r.matcher(vars.get("title"));
	
        // [ Star Trek: The Next Generation ][ 05x07 ][ Unification (1) ][ 04-Nov-1991 ]
	if(m.find())
	{
	    vars.put("series", m.group(1));
	    vars.put("season", String.valueOf(Integer.parseInt(m.group(2))));
	    vars.put("episode", m.group(3));
	    vars.put("name", m.group(4));
            vars.put("airdate", m.group(5));
	}
    }
    
    public void setVar(String key, Integer id) 
    {
        try
        {
            vars.put(key, Integer.toString(id)); 
        }
        catch(NullPointerException ex)
        {
        }
    }
    
    public void setVar(String key, String id) 
    {
        try
        {
            vars.put(key, id); 
        }
        catch(NullPointerException ex)
        {
        }
    }
    
    public void setAquired()
    {
        isAquired = true;
    }
    
    public void setSeen()
    {
        isSeen = true;
    }
    
    public String getVar(String key) 
    { 
        return vars.get(key); 
    }
    
    public boolean isAquired()
    {
        return isAquired;
    }
    
    public boolean isSeen()
    {
        return isSeen;
    }
    
    public void setNzbTree(Object tree) 
    { 
        newzNabTree = tree;  
    }
    
    public Object getNzbTree() 
    {
        return newzNabTree; 
    }
    
    public String getSeriesName() 
    { 
        return vars.get("series"); 
    }
    
    public String getSeasonNumber() 
    { 
        return vars.get("season"); 
    }
    
    public String getEpisodeNumber() 
    { 
        return vars.get("episode"); 
    }
    
    public String getEpisodeName() 
    { 
        return vars.get("name"); 
    }
    
    public String getGuid() 
    { 
        return vars.get("guid"); 
    }
    
    public String getAirDate() 
    { 
        return vars.get("airdate"); 
    }
    
    public String getSaveSeasonNumber() 
    { 
        return "Season " + vars.get("season"); 
    }
    
    public String getSaveName() 
    { 
        // Save name
        String saveName = vars.get("series") + " - " + vars.get("season") + "x" + vars.get("episode") + " - " + vars.get("name") + ".nzb";
        
        // Fix the save name
        return saveName.replaceAll("[" + vars.get("replace") + "]", "");
    }
    
    public String getSaveSeasonEpisode() 
    { 
        return vars.get("season") + "x" + vars.get("episode"); 
    }
    
    public String getToolTip() 
    { 
        return "<html>" + this.toString() + "<br>" + vars.get("airdate") + "</html>"; 
    }
    
    @Override 
    public String toString() 
    { 
        return vars.get("name") + " [ " + this.getSaveSeasonEpisode() + " ]"; 
    }
}