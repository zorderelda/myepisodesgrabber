/********************************************************************************************
 * GetMyEpisodesData.java to MyEpisodesGrabber
 *
 *  Copyright 2017 by Jeffrey W. Walsh, CD1
 *
 * This file is part of some open source application.
 * 
 * Some open source application is free software: you can redistribute 
 * it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either 
 * version 3 of the License, or (at your option) any later version.
 * 
 * Some open source application is distributed in the hope that it will 
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MyEpisodesGrabber.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license GPLv3.0 <https://bitbucket.org/zorderelda/myepisodesgrabber/>
 * 
 * Creation Date: Nov 07, 2016
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * TODO:
 * -
 * 
********************************************************************************************/

/********************************************************************************************
 * 
 * Changes:
 * v1.00
 * - Added regex check for the query replacement variable for tvmaze
 * 
********************************************************************************************/

package myEpisodesData;

import base.MyEpisodesTree;
import myEpisodesData.tvMazeJson.TvMazeJson;
import configs.ApplicationJson;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.apache.logging.log4j.LogManager;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

public class GetMyEpisodesData implements Runnable
{
    private URL link;
    private volatile MyEpisodesTree syncTree;
    private volatile ApplicationJson appObj;
    
    public GetMyEpisodesData(MyEpisodesTree tree, ApplicationJson json)
    {   
        // Copy tree
        syncTree = tree;
        
        // Assign json to obj
        appObj = json;
        
        // Init
        link = null;
        
        // Setup the link to get
        String searchString = tree.getMyEpsJsonObject().getCompiledAddress();

        // Log the data
        LogManager.getLogger("info").info(searchString);

        try 
        {
            link = new URL(searchString);
        }
        catch(MalformedURLException ex) 
        {
            LogManager.getLogger().error(GetMyEpisodesData.class.getName(), ex);
        }
    }

    @Override
    public void run() 
    {
        File tempFile = null;
        try 
        {
            tempFile = File.createTempFile("myepisodes", ".temp");
            tempFile.deleteOnExit();
            FileUtils.copyURLToFile(link, tempFile); 
        }
        catch(IOException ex) 
        {
            LogManager.getLogger().error(GetMyEpisodesData.class.getName(), ex);
        }
        
        // Add the top level tree element
	DefaultMutableTreeNode root = new DefaultMutableTreeNode("My Episodes");
        
        // Check to make sure the file has something
        if(tempFile.length() > 0)
        {
            // Process the data
            ArrayList<Show> items = new ArrayList<>();
            try 
            {
                SAXReader reader = new SAXReader();
                Document document = reader.read(tempFile);
                List<Node> nodes = document.selectNodes("/rss/channel/item");

                for(Node node : nodes)
                {
                    if(node.getName().equalsIgnoreCase("item") && !node.selectSingleNode("title").getText().equalsIgnoreCase("No Episodes"))
                        items.add(new Show(node, appObj.getMyEpisodes().getReplacement()));
                }
            }
            catch(DocumentException ex) 
            {
                LogManager.getLogger().error(GetMyEpisodesData.class.getName(), ex);
            }
        
            // Loop through and create the tree
            for(Show show : items)
            {
                // First check to see if we have the series
                DefaultMutableTreeNode seriesNode = find(root, show.getSeriesName());

                // Check to see if null and add if so
                if(seriesNode == null)
                {
                    // Create a tvmaze for this series and add it to top
                    TvMazeJson json = new TvMazeJson();

                    // Setup the string to return
                    try
                    {
                        // Get rid of all the non characters
                        String series = show.getSeriesName()
                                .replaceAll("[" + appObj.getTvMazeApiJson().getReplacement() + "]", "")
                                .replaceAll("\\s", appObj.getTvMazeApiJson().getSeparator());
                        
                        // The serarch string
                        String searchString = null;
                        
                        // Regex stuff
                        String pattern = "q=([%a-zA-Z0-9]+)";

                        Pattern r = Pattern.compile(pattern);
                        Matcher m = r.matcher(appObj.getTvMazeApiJson().getAddress());
                        if(m.find())
                            searchString = appObj.getTvMazeApiJson().getAddress().replaceAll(m.group(1), series);
                        
                        else
                            searchString = appObj.getTvMazeApiJson().getAddress().replaceAll("%SERIESNAME%", series);

                        // Log this activity
                        LogManager.getLogger("info").info(searchString);

                        // Now check to see if this is a special series name
                        Gson gson = new Gson();
                        tempFile = File.createTempFile("tvmaze", ".temp");
                        tempFile.deleteOnExit();
                        FileUtils.copyURLToFile(new URL(searchString), tempFile);
                        json = gson.fromJson(new FileReader(tempFile), TvMazeJson.class);
                    }
                    catch(IOException ex)
                    {
                        LogManager.getLogger().error(GetMyEpisodesData.class.getName(), ex);
                    }

                    // Create the series
                    seriesNode = new DefaultMutableTreeNode(json);

                    // Then create the season
                    DefaultMutableTreeNode seasonNode = new DefaultMutableTreeNode("Season " + show.getSeasonNumber());

                    // Add the TvMaze data to the show
                    show.setVar("tvmazeid", json.getId());
                    show.setVar("rid", json.getExternals().getTvrage());
                    show.setVar("tvdbid", json.getExternals().getThetvdb());
                    show.setVar("imdbid", json.getExternals().getImdb());

                    // Then add the show to the season
                    seasonNode.add(new DefaultMutableTreeNode(show));

                    // Now add the season to the series
                    seriesNode.add(seasonNode);

                    // Now add this json to the series
                    root.add(seriesNode);
                }

                // Else its there so we need to go onward to season
                else
                {
                    // First check to see if we have the series
                    DefaultMutableTreeNode seasonNode = find(seriesNode, "Season " + show.getSeasonNumber());

                    // Check to see if null and add if so
                    if(seasonNode == null)
                    {
                        // So we need to add the season
                        seriesNode.add(new DefaultMutableTreeNode("Season " + show.getSeasonNumber()));
                    }

                    // Its there so we need to add this show to the node
                    else
                    {
                        // Create a tvmaze for this series and add it to top
                        TvMazeJson json = (TvMazeJson)seriesNode.getUserObject();

                        // Add the TvMaze data to the show
                        show.setVar("tvmazeid", json.getId());
                        show.setVar("rid", json.getExternals().getTvrage());
                        show.setVar("tvdbid", json.getExternals().getThetvdb());
                        show.setVar("imdbid", json.getExternals().getImdb());

                        // Then add the show to the season
                        seasonNode.add(new DefaultMutableTreeNode(show));
                    }
                }
            }
        }
        
        // Check to see if its empty
        if(root.getChildCount() < 1)
            root.add(new DefaultMutableTreeNode("Empty..."));
        
        synchronized(syncTree)
        {
            DefaultTreeModel model = (DefaultTreeModel)syncTree.getModel();
            model.setRoot(root);
            model.reload();
        }
    }
    
    private DefaultMutableTreeNode find(DefaultMutableTreeNode root, String s) 
    {
        Enumeration<DefaultMutableTreeNode> en = root.breadthFirstEnumeration();
        while(en.hasMoreElements()) 
        {
            DefaultMutableTreeNode node = en.nextElement();
            
            if(node.getUserObject().toString().equalsIgnoreCase(s))
                return node;
        }
        
        return null;
    }
    
    private DefaultMutableTreeNode find(DefaultMutableTreeNode root, Object obj) 
    {
        Enumeration<DefaultMutableTreeNode> en = root.depthFirstEnumeration();
        while(en.hasMoreElements()) 
        {
            DefaultMutableTreeNode node = en.nextElement();
            
            if(node.equals(obj)) 
            {
                return node;
            }
        }
        
        return null;
    }
}
